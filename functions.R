

#' get currency exchange rates
#' 
#' This function returns the currency exchange rates GBP <-> EUR for the selected
#' time period
#'
#' @param start_date character, start date of investigation.
#' start_date is included for investigation. timezone format should be CET.
#' can be written in format ymd.
#' @param end_date character, end date of investigation.
#' end_date is included for investigation. timezone format should be CET.
#' can be written in format ymd. 
#'
#' @return data frame containing currency exchange rates
#' @export
get_currency_exchange_rates <- function(start_date = NA_character_,
                                        end_date = NA_character_) {
  # to gurantee valid values data is requested 7 days earlier than requested.
  start_date <- floor_date(convert_date_character(start_date) - days(7), "days")
  
  end_date <- convert_date_character(end_date)
  if (is.na(start_date)) {
    warning("Missing start date!")
  }
  if (is.na(end_date)) {
    warning("Missing end date!")
  }
  # database connection
  initialize_pool()
  exchange_rates_tbl <- tbl(db_pool, in_schema("marketdata", "exchange_rates"))
  
  suppressWarnings({
    exchange_rate <- exchange_rates_tbl %>%
      filter(.data$timestamp >= start_date, .data$timestamp < end_date) %>%
      collect()
  })
  # query data
  if (plyr::empty(exchange_rate)) {
    warning("no data found!")
  } else {
    print_info_message("data collected!")
  }
  # prepare data
  exchange_rate <- exchange_rate %>%
    convert_column_names_to_snakecase() %>%
    mutate(timestamp = ymd_hms(.data$timestamp, tz = "UTC"))
  
  time_line <- create_time_line_data_frame(start_date, end_date,
                                           duration = 24 * 3600, column_name = "timestamp",
                                           time_zone = "UTC"
  )
  
  time_line <- time_line %>%
    uncount(weights = 2, .id = "id_tmp") %>%
    mutate(source_currency = if_else(.data$id_tmp == 1, "GBP", "EUR")) %>%
    mutate(target_currency = if_else(.data$id_tmp == 1, "EUR", "GBP")) %>%
    select(-.data$id_tmp)
  
  exchange_rate <- time_line %>%
    left_join(exchange_rate, by = c("timestamp", "source_currency", "target_currency")) %>%
    group_by(.data$source_currency, .data$target_currency) %>%
    arrange(.data$timestamp) %>%
    fill(.data$rate) %>%
    ungroup() %>%
    # to guarantee valid values data is requested 7 days earlier than requested.
    # FIX if you add 7 days, you miss your first day
    filter(.data$timestamp >= (start_date + days(6))) %>%
    ### inserted mutate and pivot_wider line    
    mutate(day = .data$timestamp) %>%
    pivot_wider(id_cols = "day", names_from = c(source_currency, target_currency),
                values_from = rate)
  
  return(exchange_rate)
}




#' get bid results data frame for specific border(s) and interconnector(s)
#'
#' This function returns a data frame containing information about the auction
#' identification, delivery period, our initial bid quantities and prices,
#' our allocated capacity and the capacity price
#'
#' @param start_date character, start date of investigation.
#' start_date is included for investigation. timezone format should be CET.
#' can be written in format ymd or ymd_hms.
#' @param end_date character, end date of investigation.
#' end_date is included for investigation. timezone format should be CET.
#' can be written in format ymd or ymd_hms.
#' @param border_filter character representing border(s) to filter,
#' in format "source_target" ("GB_NL")
#' @param interconnector_filter character representing interconnector(s) to filter,
#' must be in ("IFA", "IFA2", "BN", "NEMO")
#'
#' @importFrom optimaxutils add_uk_border_name_column
#' @importFrom optimaxutils add_interconnector_name_column
#'
#' @return data frame containing bid results information
#' @export
get_bid_results <- function(start_date,
                            end_date,
                            border_filter = NA_character_,
                            interconnector_filter = NA_character_) {
  start_date <- convert_date_character(start_date)
  end_date <- convert_date_character(end_date)
  if (is.na(start_date) | is.na(end_date)) {
    stop("get_bid_results: wrong date format input! \n")
  }
  
  
  initialize_pool()
  
  bid_results_tbl <- tbl(
    db_pool,
    in_schema("capacity", "bidresults")
  )
  
  
  suppressWarnings({
    bid_results_data <- bid_results_tbl %>%
      filter(deliveryStart >= start_date) %>%
      filter(deliveryStart < end_date) %>%
      collect()
  })
  
  
  if (plyr::empty(bid_results_data)) {
    cat("get_bid_results: no data found on data base! \n")
  } else {
    cat("get_bid_results: data collected! \n")
  }
  
  bid_results_data <- bid_results_data %>%
    transmute(
      delivery_start = with_tz(ymd_hms(deliveryStart), tz = "CET"),
      delivery_end = with_tz(ymd_hms(deliveryEnd), tz = "CET"),
      border = get_uk_border_name_from_character(auctionId),
      interconnector = get_interconnector_name_from_character(auctionId),
      bid_quantity = initialQuantity / 1000,
      bid_price = bidPrice / 100,
      allocated_quantity = quantity / 1000,
      capacity_price = price / 100
    ) %>%
    filter(border %in% border_filter | is.na(border_filter)) %>%
    filter(interconnector %in% interconnector_filter |
             is.na(interconnector_filter))
  
  
  return(bid_results_data)
}




#' get capacity price for UK interconnector
#'
#' This function returns the capacity price for any UK - EU border and any
#' UK - EU interconnector and the optimax allocated capacity
#' using the "get_bid_results" function
#'
#' @param start_date character, start date of investigation.
#' start_date is included for investigation. timezone format should be CET.
#' can be written in format ymd or ymd_hms.
#' @param end_date character, end date of investigation.
#' end_date is included for investigation. timezone format should be CET.
#' can be written in format ymd or ymd_hms.
#' @param border_filter character representing border(s) to filter,
#' in format "source_target" ("GB_NL")
#' @param IC_filter character representing interconnector(s) to filter,
#' must be in ("IFA", "IFA2", "BN", "NEMO")
#'
#'
#' @return data frame containing capacity price and optimax allocated capacity
#' @export
get_capacity_price <- function(start_date,
                               end_date,
                               border_filter = NA_character_,
                               interconnector_filter = NA_character_) {
  capacity_price_data <- get_bid_results(
    start_date,
    end_date,
    border_filter,
    interconnector_filter
  )
  
  
  if (plyr::empty(capacity_price_data)) {
    cat("get_capacity_price: no data found on data base! \n")
  } else {
    cat("get_capacity_price: data collected! \n")
  }
  
  
  capacity_price_data <- capacity_price_data %>%
    group_by(delivery_start, border, interconnector) %>%
    summarise(
      allocated_capacity = sum(allocated_quantity, na.rm = TRUE),
      capacity_price = max(capacity_price, na.rm = TRUE)
    ) %>%
    ungroup()
  
  capacity_price_data[is.na(capacity_price_data)] <- 0
  
  
  return(capacity_price_data)
}


